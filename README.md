Basic starter to get up and running with Eleventy and Tailwind. Basic functionality with robots.txt, sitemap.xml and feed.xml.

## Features

- Template Example with Nunjucks
- Tailwind JIT
- Tailwind Typography
- robots.text
- sitemap.xml
- HTML minifier
- PostCSS
- Atom feed

## Installation

Install all dependencies:

```npm install
```

## Using

Change the site settings in `src/_data/site.json`.

To serve and watch for changes:

```npm start
```

To build the site and get a minified version of the html and css:

```npm run build
```