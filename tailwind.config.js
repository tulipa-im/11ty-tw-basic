const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  purge: [
    './_site/**/*.html',
    './src/**/*.njk',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: [
          'Lato',
          ...defaultTheme.fontFamily.sans,
        ],
        serif: [
          'Merriweather',
          ...defaultTheme.fontFamily.serif,
        ],
        mono: [
          'Fira Code',
          ...defaultTheme.fontFamily.mono,
        ]        
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            h1: {
              fontFamily: [theme('fontFamily.sans')]
            },
            h2: {
              fontFamily: [theme('fontFamily.sans')]
            },
            h3: {
              fontFamily: [theme('fontFamily.sans')]
            },
            h4: {
              fontFamily: [theme('fontFamily.serif')]
            },
            h5: {
              fontFamily: [theme('fontFamily.serif')]
            },
            h6: {
              fontFamily: [theme('fontFamily.serif')]
            }
          }
        }
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
