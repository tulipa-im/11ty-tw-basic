const postcssConfig = {
  plugins: {
    "postcss-import": {},
    tailwindcss: {},
    autoprefixer: {},
         
  }
}

// If we are in production mode, then add cssnano
if (process.env.NODE_ENV === 'production') {
	postcssConfig.plugins.cssnano = {
    preset: 'default'
  } 
}

module.exports = postcssConfig;