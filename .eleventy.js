const rssPlugin = require('@11ty/eleventy-plugin-rss');

// Import transforms
const htmlMinTransform = require("./_transforms/html-minify.js");

module.exports = (eleventyConfig) => {

  // https://www.11ty.dev/docs/ignores/
  eleventyConfig.setUseGitIgnore(false);

  // https://www.11ty.dev/docs/data-deep-merge/
  eleventyConfig.setDataDeepMerge(true);

  // When styles change, reload site
  eleventyConfig.addWatchTarget("./src/_css/");

  // Passthrough
  eleventyConfig.addPassthroughCopy({"./src/_fonts": "./assets/fonts"});
  eleventyConfig.addPassthroughCopy({"./src/_images": "./assets/images"});

  // Transforms
  if (process.env.NODE_ENV == "production") {
    eleventyConfig.addTransform("htmlmin", htmlMinTransform);
  }

  // Plugins
  eleventyConfig.addPlugin(rssPlugin);

  return {
      dir: {
        input: "src"
      }
    } 
}